
import java.rmi.Naming;

public class Cliente {
 	
	private static final String NOME_SERVICO = "SERVIDOR_RMI";
	
	public Cliente(String h, String args){
				
		System.out.println(executarTarefaRemota(h, args));
		
	}//fim construtor	
		
	public static void main(String args[]){
		
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
		new Cliente(args[0],args[1]);		
		
	}//fim main
	
	public String executarTarefaRemota(String host, String argumentos){
		
		//A tarefa foi executada com sucesso quando o retorno for "0" 
		String resultado = "1";
			
		IServidor servicoCliente = null;
						
		try{
			String caminhoServico = "rmi://" + host + "/" + NOME_SERVICO;			
			
                        //Obtem a referencia do servico remoto
			servicoCliente = (IServidor) Naming.lookup(caminhoServico);
			
			try {
				
				//Solicita a execucao remota do metodo 
				//que devera ser realizada pelo servico remoto
				resultado = servicoCliente.executarTarefa(argumentos);
							
			} catch ( Exception e ){
				System.out.println("Excecao no cliente RMI:" + e.getMessage());
			}//fim catch
			
        } catch ( Exception e ){
			System.out.println("Excecao ao obter a referencia remota do servico:" + e.getMessage());
			
		}//fim catch

		return resultado;
		
	}//fim executarTarefaRemota
		
}//fim classe
