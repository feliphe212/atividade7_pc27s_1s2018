
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


public class Servidor extends UnicastRemoteObject implements IServidor {
	
	private static final long serialVersionUID = 1L;
	private static final int PORTA_REGISTRO = 1099;
	private static final String NOME_SERVICO = "SERVIDOR_RMI";


	public Servidor() throws RemoteException {
		//Eh necessario ter um construtor aqui para
		//invocar o metodo construtor da superclasse
		super();		
	}	

	public static void main(String args[]){		
		
		//Gerenciador de Seguranca
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
		
		try {
			//Criar o registry na porta especificada
			Registry registry = LocateRegistry.createRegistry(PORTA_REGISTRO);
			
			//Criar o servidor
			Servidor servidor = new Servidor();
			
			//Registrar o servidor
			registry.rebind(NOME_SERVICO, servidor);
			
			System.out.println("Servidor RMI em " + System.getenv("HOSTNAME") + " ativado.");
			
		} catch ( Exception e ){
			
			System.out.println(e.getMessage());			
			
		}//fim catch
		
	}//fim main
       

	public String executarTarefa(String args) throws RemoteException {		

	    String resultado = "";

	    //Se o cliente fornecer como argumento a string 1
	    if (args.equals("1"))
		resultado = "Primeira opcao. Valor recebido do cliente: " + args;
	    else
		resultado = "Segunda opcao. Valor recebido do cliente: " + args;

	    return resultado;
	
    }//fim executarTarefa
	
}//fim Servidor
